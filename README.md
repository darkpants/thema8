### Commands for uploading files ###

*	 - git status
*		this will check if your files are synced with the online repository

*	 - git add <filename> (can be multiple files, you can see all the files available for upload with git status)
*		adds a file from your local direcotry for pushing to the online repository

*	 - git commit -m "add your message here"
*		this adds a message to the commit for clarification in the online repository so its clear what changes you have made
*		note: the message will be added to all the files you commit in one go, try using multiple commits for multiple changes for a clear overview

*	 - git push
*		this will push your files to the online repository
	
### Commands for downloading files ###

*	 - git pull
*		this will update all the files if changes were made
